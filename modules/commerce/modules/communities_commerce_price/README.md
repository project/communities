If you want to view the resolved price on the product entity, you must use the calculated pricing formatter in the
Manage display settings.
