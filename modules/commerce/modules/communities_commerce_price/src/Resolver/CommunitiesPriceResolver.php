<?php

namespace Drupal\communities_commerce_price\Resolver;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Resolver\PriceResolverInterface;

/**
 *
 */
class CommunitiesPriceResolver implements PriceResolverInterface {

  /**
   * Resolves a price for the given purchasable entity.
   *
   * Use $context->getData('field_name', 'price') to get the name of the field
   * for which the price is being resolved (e.g "list_price", "price").
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   * @param int $quantity
   *   The quantity.
   * @param \Drupal\commerce\Context $context
   *   The context.
   *
   * @return \Drupal\commerce_price\Price|null
   *   A price value object, if resolved. Otherwise NULL, indicating that the
   *   next resolver in the chain should be called.
   */
  public function resolve(PurchasableEntityInterface $entity, $quantity, Context $context) {
    /** @var \Drupal\communities\CommunityManagerInterface $community_manager */
    $community_manager = \Drupal::service('community_manager');
    $user_community = $community_manager->getCurrentCommunity();
    $pricing_entities = $entity->get('communities_price')->referencedEntities();
    foreach ($pricing_entities as $pricing_entity) {
      $price_communities = $pricing_entity->getCommunities();
      foreach ($price_communities as $price_community) {
        if ($user_community == $price_community['value']) {
          return $pricing_entity->getPrice();
        }
      }
    }
  }

}
