<?php

namespace Drupal\communities_commerce_price;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Community entities.
 *
 * @ingroup communities
 */
class CommunityPriceListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Community ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\communities\Entity\Community $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->id(),
      'entity.community_price.edit_form',
      ['community_price' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
