<?php

namespace Drupal\communities_commerce_price\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Community Price entity.
 *
 * @ingroup communities_commerce_price
 *
 * @ContentEntityType(
 *   id = "community_price",
 *   label = @Translation("Community Price"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\communities_commerce_price\CommunityPriceListBuilder",
 *     "form" = {
 *       "default" = "Drupal\communities_commerce_price\Form\CommunityPriceForm",
 *       "add" = "Drupal\communities_commerce_price\Form\CommunityPriceForm",
 *       "edit" = "Drupal\communities_commerce_price\Form\CommunityPriceForm",
 *       "delete" = "Drupal\communities_commerce_price\Form\CommunityPriceDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\communities_commerce_price\CommunityPriceHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\communities_commerce_price\CommunityPriceAccessControlHandler",
 *   },
 *   base_table = "community_price",
 *   data_table = "community_price_field_data",
 *   admin_permission = "administer community price entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "label"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/community-price/{community_price}",
 *     "add-form" = "/admin/structure/community-price/add",
 *     "edit-form" = "/admin/structure/community-price/{community_price}/edit",
 *     "delete-form" = "/admin/structure/community-price/{community_price}/delete",
 *     "collection" = "/admin/structure/community-price",
 *   }
 * )
 */
class CommunityPrice extends ContentEntityBase implements CommunityPriceInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The label of this override.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['communities'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Communities'))
      ->setDescription(t('Communities that share the same price.'))
      ->setRequired(TRUE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('allowed_values_function', 'communities_allowed_values_function_regions')
      ->setDisplayOptions('form', [
        'type' => 'options_tree',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['price'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Community price'))
      ->setDescription(t('The price this override should contain.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'commerce_price_default',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'commerce_price_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   *
   */
  public function getCommunities() {
    return $this->get('communities')->getValue();
  }

  /**
   *
   */
  public function getPrice() {
    if (!$this->get('price')->isEmpty()) {
      return $this->get('price')->first()->toPrice();
    }
  }

}
