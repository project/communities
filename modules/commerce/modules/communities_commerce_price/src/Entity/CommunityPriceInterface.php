<?php

namespace Drupal\communities_commerce_price\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Community entities.
 *
 * @ingroup communities
 */
interface CommunityPriceInterface extends ContentEntityInterface {

  /**
   *
   */
  public function getCommunities();

  /**
   *
   */
  public function getPrice();

}
