<?php

namespace Drupal\communities_commerce_price;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Community entity.
 *
 * @see \Drupal\communities\Entity\Community.
 */
class CommunityPriceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\communities\Entity\CommunityInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view community price entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit community price entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete community price entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add community price entities');
  }

}
