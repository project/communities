<?php

namespace Drupal\communities_commerce_price\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Community edit forms.
 *
 * @ingroup communities
 */
class CommunityPriceForm extends ContentEntityForm {

  /**
   * The Communities Region Manager.
   */
  protected $communitiesRegionManager;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Constructs a new CommunityForm.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user account.
   */
  // Public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, AccountProxyInterface $account, CommunitiesRegionManagerInterface $communities_region_manager) {
  //  parent::__construct($entity_repository, $entity_type_bundle_info, $time);
  //
  //  $this->account = $account;
  //  $this->communitiesRegionManager = $communities_region_manager;
  // }
  //
  // **
  // * {@inheritdoc}
  // */
  // public static function create(ContainerInterface $container) {
  //  // Instantiates this form class.
  //  return new static(
  //    $container->get('entity.repository'),
  //    $container->get('entity_type.bundle.info'),
  //    $container->get('datetime.time'),
  //    $container->get('current_user'),
  //    $container->get('communities_region_manager')
  //  );
  // }.

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var Community $entity */
    $form = parent::buildForm($form, $form_state);

    // $regions = $this->communitiesRegionManager->getRegions();
    //  if (empty($regions)){
    //    $region_route = Url::fromRoute('entity.communities_region.add_form')->toString();
    //    $region_link = "<a href='$region_route'>Region</a>";
    //    $message = t('There are no ' . $region_link . ' entities created yet. They are required for Communities.');
    //    \Drupal::messenger()->addWarning($message, TRUE);
    //    $form['actions']['submit']['#disabled'] = TRUE;
    //  }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    // Update the default config variable once there is a community.
    $default_community = \Drupal::config('communities.settings')->get('default_community');
    if ($default_community == NULL) {
      \Drupal::configFactory()->getEditable('communities.settings')->set('default_community', $entity->id())->save();
    }
    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Community.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Community.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.communities_region.overview_form');
  }

}
