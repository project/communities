<?php

namespace Drupal\communities_commerce_price\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Community entities.
 *
 * @ingroup communities
 */
class CommunityPriceDeleteForm extends ContentEntityDeleteForm {

}
