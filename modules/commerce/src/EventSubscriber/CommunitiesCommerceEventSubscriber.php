<?php

namespace Drupal\communities_commerce\EventSubscriber;

use Drupal\communities\Event\CommunitiesEvent;
use Drupal\communities\Event\CommunitiesEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 */
class CommunitiesCommerceEventSubscriber implements EventSubscriberInterface {

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * The array keys are event names and the value can be:
   *
   *  * The method name to call (priority defaults to 0)
   *  * An array composed of the method name to call and the priority
   *  * An array of arrays composed of the method names to call and respective
   *    priorities, or 0 if unset
   *
   * For instance:
   *
   *  * ['eventName' => 'methodName']
   *  * ['eventName' => ['methodName', $priority]]
   *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
   *
   * @return array The event names to listen to
   */
  public static function getSubscribedEvents() {
    $events[CommunitiesEvents::COMMUNITY_UPDATE][] = ['processCommunityChange'];
    return $events;
  }

  /**
   * Clear carts when community is changed.
   */
  public function processCommunityChange(CommunitiesEvent $event) {
    $this->clearCurrentCarts();
  }

  /**
   * Clears the current user's carts.
   */
  public function clearCurrentCarts() {
    /** @var \Drupal\commerce_cart\CartManagerInterface $cart_manager */
    $cart_manager = \Drupal::service('commerce_cart.cart_manager');

    /** @var \Drupal\commerce_cart\CartProviderInterface $cart_provider */
    $cart_provider = \Drupal::service('commerce_cart.cart_provider');

    $current_account = \Drupal::currentUser()->getAccount();
    $carts = $cart_provider->getCarts($current_account);

    foreach ($carts as $cart) {
      $cart_manager->emptyCart($cart);
    }
  }

}
