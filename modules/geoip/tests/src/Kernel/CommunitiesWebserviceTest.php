<?php

namespace Drupal\Tests\communities_geoip\Kernel;

use Drupal\Core\Site\Settings;
use Drupal\Tests\token\Kernel\KernelTestBase;

/**
 * Tests the default GeoLocator plugins.
 *
 * @group geoip
 */
class CommunitiesWebserviceTest extends KernelTestBase {

  public static $modules = [
    'geoip',
    'communities_geoip',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->installConfig(['geoip']);

    $this->config('geoip.geolocation')
      ->set('plugin_id', 'communities_webservice')
      ->save();
    Settings::get('geoip_account_id');
    $this->config('geoip.geolocation')->set('account_id', '')->save();
    $this->config('geoip.geolocation')->set('license_key', '')->save();
  }

  /**
   * Tests the webservice plugin.
   */
  public function testWebservice() {
    $this->assertEquals('Kelowna', $this->container->get('geoip.geolocation')->geolocate('205.250.176.133'));
  }

}
