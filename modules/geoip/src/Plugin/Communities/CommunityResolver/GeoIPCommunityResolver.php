<?php

namespace Drupal\communities_geoip\Plugin\Communities\CommunityResolver;

use Drupal\communities\Plugin\Communities\CommunityResolver\CommunityResolverPluginBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Geo-ip community resolver.
 *
 * @CommunityResolver(
 *   id = "geo_ip_community_resolver",
 *   label = @Translation("GeoIP"),
 *   description = @Translation("Automatically pick the community based on IP. If not found, trigger pop-up.")
 * )
 */
class GeoIPCommunityResolver extends CommunityResolverPluginBase {

  /**
   * Resolves community based on IP, trigger pop-up if not.
   *
   * @param $event
   *   The Event.
   *
   * @return int
   *   The community ID value.
   */
  public function resolveCommunity($event) {
    // Invalidate system cache so that pop-up library behaves correctly.
    // @todo this is sort-of a hack - maybe? I wasn't able to figure out a better way. The JS library was messing it up.
    \Drupal::service('cache_tags.invalidator')->invalidateTags(['config:system.site']);

    /** @var \Drupal\geoip\GeoLocation $geo_locator */
    $geo_locator = \Drupal::service('geoip.geolocation');
    $ip = \Drupal::request()->getClientIp();
    $city = $geo_locator->geolocate($ip);
    $city = strtolower($city);
    $community = \Drupal::service('community_manager')->getCommunityFromName($city);

    if (!empty($community)) {
      $response = new RedirectResponse(Url::fromRoute('<current>')->toString());
      /** @var \Drupal\communities\CommunityManagerInterface $community_manager */
      $community_manager = \Drupal::service('community_manager');
      $response = $community_manager->setCurrentCommunityResponse($response, reset($community)->id());
      $event->setResponse($response);
      return $event;
    }
  }

}
