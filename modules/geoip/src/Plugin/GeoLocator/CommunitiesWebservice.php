<?php

namespace Drupal\communities_geoip\Plugin\GeoLocator;

use Drupal\geoip\Plugin\GeoLocator\Webservice;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\WebService\Client;

/**
 * Local geolocation provider.
 *
 * @GeoLocator(
 *   id = "communities_webservice",
 *   label = "Communities Webservice",
 *   description = "Uses the MaxmindDB webservice for geolocation. Resolves to cities.",
 *   weight = 11
 * )
 */
class CommunitiesWebservice extends Webservice {

  /**
   * Geolocate().
   */
  public function geolocate($ip_address) {
    $client = $this->getClient();
    // If the client could not be initiated, then back out.
    if (!$client instanceof Client) {
      return NULL;
    }

    try {
      $record = $client->city($ip_address);

      if ($this->geoIpConfig->get('debug')) {
        $this->logger->notice($this->t('Found %ip_address in the Maxmind webservice', [
          '%ip_address' => $ip_address,
        ]));
      }

      return $record->city->name;
    }
    catch (AddressNotFoundException $e) {
      if ($this->geoIpConfig->get('debug')) {
        $this->logger->notice($this->t('Unable to find %ip_address in the Maxmind webservice', [
          '%ip_address' => $ip_address,
        ]));
      }
      return NULL;
    }
    catch (\Exception $e) {
      $this->logger->error($this->t('Unable to query the Maxmind webservice.'));
      return NULL;
    }
  }

}
