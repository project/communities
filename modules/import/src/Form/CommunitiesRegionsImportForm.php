<?php

namespace Drupal\communities_import\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\csv_importer\Form\ImporterForm;

/**
 * Class CommunitiesRegionsImportForm().
 */
class CommunitiesRegionsImportForm extends ImporterForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    unset($form['importer']['entity_type']);

    if ($this->entitiesExist()) {
      // Manually add error so ajax doesn't render the Drupal::messenger twice.
      $form['warning'] = [
        '#type' => 'markup',
        '#markup' => '<div class="messages messages--error">WARNING: Running this import will PERMANENTLY ERASE ALL Community and Region Entities which may leave the site in an undesirable state.<br>They will not be recoverable. Ensure you have a backup if you wish to avoid some issues.</div>',
        '#weight' => -1,
      ];

      $form['confirmation'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('I understand importing will permanently remove all current Community and Region entities.'),
        '#required' => TRUE,
      ];

      $form['actions']['submit']['#states'] = [
        'visible' => [
          ':input[name="confirmation"]' => ['checked' => TRUE],
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $csv = current($form_state->getValue('csv'));
    $csv_parse = $this->parser->getCsvById($csv, ',');

    $regions = $communities = [];
    foreach ($csv_parse as $row => $columns) {
      $communities[] = $columns[0];
      $region_name = $this->removeWhitespace($columns[1]);
      $regions[] = $region_name;
    }
    $regions = array_unique($regions);

    $batch = [
      'title' => $this->t('Importing Communities & Regions...'),
      'operations' => [
        [
          [$this, 'deleteExistingEntities'], [],
        ],
        [
          [$this, 'importRegions'], [$regions],
        ],
        [
          [$this, 'importCommunities'], [$csv_parse],
        ],
      ],
      'init_message' => $this->t('Importing'),
      'progress_message' => $this->t('Processed @current out of @total.'),
      'error_message' => $this->t('An error occurred during processing.'),
      'finished' => $this->t('Finished'),
    ];
    batch_set($batch);
  }

  /**
   * Returns whether community or region entities exist.
   */
  public function entitiesExist() {
    $storage = \Drupal::entityTypeManager()->getStorage('communities_region');
    if (!empty($storage->loadMultiple())) {
      return TRUE;
    }
    $storage = \Drupal::entityTypeManager()->getStorage('community');
    if (!empty($storage->loadMultiple())) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Deletes existing entities.
   *
   * @param $context
   *   An associative array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteExistingEntities(&$context) {
    $this->deleteAllEntities("community");
    $this->deleteAllEntities("communities_region");
  }

  /**
   * Does an entity query to grab all entities of type, and deletes them.
   *
   * @param $entity_type
   *   string The entity type to remove.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteAllEntities($entity_type) {
    $result = \Drupal::entityQuery($entity_type)
      ->accessCheck(FALSE)
      ->execute();
    $storage_handler = \Drupal::entityTypeManager()->getStorage($entity_type);
    $entities = $storage_handler->loadMultiple($result);
    $storage_handler->delete($entities);
  }

  /**
   * Creates the region entities from the CSV.
   */
  public function importRegions($regions, &$context) {
    /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $entity_storage  */
    $entity_storage = $this->entityTypeManager->getStorage('communities_region');
    foreach ($regions as $index => $region) {
      if ($index == 0) {
        continue;
      }
      $entity_storage->create(['name' => $region])->save();
    }
  }

  /**
   * Creates the community entities from the CSV.
   */
  public function importCommunities($csv_parse, &$context) {
    $region_storage = $this->entityTypeManager->getStorage('communities_region');
    $regions = $region_storage->loadMultiple();
    $community_storage = $this->entityTypeManager->getStorage('community');
    foreach ($csv_parse as $index => $row) {
      if ($index == 0) {
        continue;
      }
      foreach ($regions as $region) {
        $csv_region_name = $this->removeWhitespace($row[1]);
        if ($csv_region_name == $region->getName()) {
          $community_name = $this->removeWhitespace($row[0]);
          $community_storage->create(['name' => $community_name, 'communities_region' => $region->id()])->save();
        }
      }
    }
  }

  /**
   * Removes whitespace given a string.
   */
  public function removeWhitespace($input) {
    $input = trim($input);
    $input = preg_replace('!\s+!', ' ', $input);
    return $input;
  }

}
