<?php

namespace Drupal\communities_node;

/**
 *
 */
interface CommunitiesNodeManagerInterface {

  /**
   * Returns an array of communities that belong to the node.
   *
   * This should return the communities part of a group, if group is selected for the node.
   *
   * @param $node_id
   *   string the node_id to load.
   *
   * @return array the communities to be controlled by node.
   */
  public function getNodeCommunities($node_id);

}
