<?php

namespace Drupal\communities_node;

use Drupal\communities\CommunitiesGroupManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 *
 */
class CommunitiesNodeManager implements CommunitiesNodeManagerInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface*/
  protected $entityTypeManager;

  /**
   * @var \Drupal\communities\CommunitiesGroupManagerInterface*/
  protected $communityGroupManager;

  /**
   * Constructs a CommunityManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\communities\CommunitiesGroupManagerInterface $communities_group_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CommunitiesGroupManagerInterface $communities_group_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->communityGroupManager = $communities_group_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeCommunities($node_id) {
    // @todo replace this with the generic method in CommunityManager.
    $node = $this->entityTypeManager->getStorage('node')->load($node_id);
    $communities_to_return = [];
    // Communities.
    if ($node->get('regions_or_groups')->value == '0') {
      foreach ($node->get('community')->getValue() as $community) {
        $communities_to_return[$community['value']] = $community['value'];
      }
    }
    else {
      foreach ($node->get('group_options')->getValue() as $group) {
        $group = $group['value'];
        // Get the communities that belong to the group.
        $group_communities = $this->communityGroupManager->getGroupCommunitiesById($group);
        foreach ($group_communities as $community) {
          $communities_to_return[$community] = $community;
        }
      }
    }

    return $communities_to_return;
  }

}
