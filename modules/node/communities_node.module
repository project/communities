<?php

/**
 * @file
 * Contains communities_view.module.
 */

use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Implements hook_entity_base_field_info().
 */
function communities_node_entity_base_field_info(EntityTypeInterface $entity_type) {
  if ($entity_type->id() == 'node') {
    $fields = communities_community_entity_field_controls();
    return $fields;
  }
}

/**
 *
 */
function communities_node_allowed_values_function_regions_or_communities() {
  return [t('Regions & Communities'), t('Groups')];
}

/**
 * Implements hook_node_access().
 */
function communities_node_node_access(NodeInterface $node, $op, AccountInterface $account) {
  if (!$account->hasPermission('bypass community filter') || !in_array('administrator', $account->getRoles())) {
    switch ($op) {

      case 'view':
        // Allowed to view if the users community is in the contents community list.
        // If the content has no community, available to everyone?
        $user_community = \Drupal::service('community_manager')->getCurrentCommunity();

        $access = TRUE;
        // -1 is set when no communities exist, but the module is enabled.
        if ($user_community != -1) {
          $access = FALSE;
          $node_communities = \Drupal::service('communities_node_manager')->getNodeCommunities($node->id());

          // Also check if there are NO communities assigned, which is only possible if content exists before enabling
          // the communities module suite, so do not limit access.
          if (in_array($user_community, $node_communities) || empty($node_communities)) {
            $access = TRUE;
          }
        }
        if ($access) {
          return AccessResult::allowed();
        }
        else {
          return AccessResult::forbidden('Community does not match.');
        }
      default:

        // No opinion.
        return AccessResult::neutral();
    }
  }
}

/**
 * Implements hook_allowed_values_function().
 */

/**
 * Function communities_node_allowed_values_function() {
 * return \Drupal::service('communities_region_manager')->getRegionCommunitiesMap();
 * }.
 */
function communities_node_form_node_form_alter_validate($form, FormStateInterface $form_state) {
  $selected_radio = $form_state->getValue('regions_or_groups')[0]['value'];
  if ($selected_radio != NULL) {
    // If regions & communities is picked, unset the other one.
    // If regions & communities is picked but no regions or communities, fail.
    // Likewise for other.
    // Already errors.
    if (!empty($form_state->getErrors())) {
      return;
    }

    // R & C.
    if ($selected_radio == '0') {
      // Unset the groups options
      // Ensure that R & C is picked.
      $form_state->setValue('group_options', []);

      if (empty($form_state->getValue('community'))) {
        $form_state->setError($form['regions_or_groups'], t('You must pick one or more community that this piece of content belongs to.'));
      }
    }
    // Groups.
    else {
      // Unset the groups options
      // Ensure that R & C is picked.
      $form_state->setValue('community', []);

      if (empty($form_state->getValue('group_options'))) {
        $form_state->setError($form['regions_or_groups'], t('You must pick one or more groups that this piece of content belongs to.'));
      }
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function communities_node_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['group_options']['#states'] = [
    // Show this textfield only if the radio 'other' is selected above.
    'visible' => [
      ':input[name="regions_or_groups"]' => ['value' => '1'],
    ],
  ];

  $form['community']['#states'] = [
    // Show this textfield only if the radio 'other' is selected above.
    'visible' => [
      ':input[name="regions_or_groups"]' => ['value' => '0'],
    ],
  ];

  $form['#validate'][] = 'communities_node_form_node_form_alter_validate';
}

/**
 * Implements hook_entity_presave().
 */
function communities_node_entity_presave(EntityInterface $entity) {
  if ($entity instanceof Node) {
    $regions_or_groups = $entity->get('regions_or_groups')->value;
    // Groups.
    if ($regions_or_groups == 1) {
      $node_communities = [];
      $groups_selected = $entity->get('group_options')->getValue();
      foreach ($groups_selected as $group_selected) {
        $group_id = $group_selected['value'];
        $group_communities = \Drupal::service('communities_group_manager')->getGroupCommunitiesById($group_id);
        if (!empty($group_communities)) {
          foreach ($group_communities as $group_community) {
            // Don't add duplicates.
            if (!in_array($group_community, $node_communities)) {
              $node_communities[$group_community] = $group_community;
            }
          }
        }
      }
      if (!empty($node_communities)) {
        $entity->set('community', $node_communities);
      }
    }
  }
}
