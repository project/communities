<?php

namespace Drupal\Tests\communities_node\Functional;

use Drupal\Core\Url;
use Drupal\Tests\node\Functional\NodeTestBase;
use Drupal\Tests\node\Traits\NodeCreationTrait;

/**
 *
 */
class LoadTest extends NodeTestBase {

  use NodeCreationTrait;

  protected $defaultTheme = 'stark';


  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['communities_node'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  // Protected $node;.

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($this->user);

    $this->node = $this->drupalCreateNode();
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  // Public function testLoad() {
  //    $this->drupalGet(Url::fromRoute('<front>'));
  //    $this->assertSession()->statusCodeEquals(200);

  /**
   * }.
   */
  public function testAccess() {
    $session = $this->getSession();
    $session->setCookie('community', NULL);
    $this->drupalGet(Url::fromRoute('<front>'));
    $node = $this->drupalCreateNode();
    $this->drupalGet($node->toUrl());
  }

}
