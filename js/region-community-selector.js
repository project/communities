(function ($, Drupal) {
  Drupal.behaviors.regionCommunitySelector = {
    attach: function (context, settings) {
      // On change, make sure we scrub the list and update to our new selected region.
      $('select#edit-region', context).change('change', function () {
        // The select control.
        var selectOption = $(this).val();

        // Un-hide the community element.
        $('#edit-community').show();
        var dropdown = $('#edit-community');

        // Clear the select list on change, so we don't always append values.
        dropdown.find('option').remove();

        // Go over all the regions
        Object.keys(settings.region_map).forEach(function(key) {
          // If the selected option matches the key, populate with the key's values.
          if(key === selectOption){
            // Go over all the communities inside the region.
            Object.keys(settings.region_map[key]).forEach(function(v){
              dropdown.append($("<option />").val(v).text(settings.region_map[key][v]));
            });
          }
        });
        if($("#edit-community option[value='"+settings.default_community+"']").length > 0)
        dropdown.val(settings.default_community);
      });

      // On load, make sure we clear the values and default to the default ones.
      $('select#edit-region', context).once('remove-options').each(function () {
        $('select#edit-region')
          .val(settings.default_region)
          .trigger('change');
      });
    }
  };
})(jQuery, Drupal);
