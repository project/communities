(function (window, Drupal, $) {
  Drupal.behaviors.optionsTree = {
    attach: function attach(context, settings) {
      var options = $(".options-tree input");

      // Make sure we only act on one of the options trees.
      var regions_or_groups = $('input[name="regions_or_groups"]:checked').val() === '0' ? 'regions' : 'groups';
      $('input[name="regions_or_groups"]', context).change("change", function(){
        regions_or_groups = $(this).val() === '0' ? 'regions' : 'groups';
        options = $(".options-tree." + regions_or_groups+" input");

        $(options).each(function(option, optionkey){
          $(this).prop("checked", false);
        });
      });

      // Only load this behavior once.
      $('.options-tree', context).once('optionsTreeSelect').each(function() {
        // All the options..

        // Only run the nice-to have's when there is one active table. or we don't know which table to act on.
        if($('table.options-tree').length === 1) {
          /**
           * This runs on load, and checks to see if all children are selected, then select the parent.
           */
          $(options).once().each(function (option, optionkey) {
            var option_value = $(optionkey).val();
            var isparent = isParent(option_value);
            if (isparent) {
              var children = getChildrenFromParent(option_value);
              var allselected = true;

              for (var child in children) {
                if (!$('tr[option-tree-data="option-' + children[child] + '"] input').prop("checked")) {
                  allselected = false;
                }
              }

              if (children.length === 0)
                allselected = false;

              if (allselected) {
                $(this).prop("checked", true);
              }
            }
          });
        }

        /**
         * This runs every time an option is changed, and checks to see if we should be selecting all children or deselecting
         * the parent based on the children.
         *  1. The parent is selected, select all children.
         *  2. All children are selected, select the parent.
         *  3. All children were selected, then at least one deselected, deselect the parent.
         */
        $(options).change("change", function(){
          var tables = $(this).parents().closest('table');
          var table = $(tables)[$(tables).length - 1];
          // Parent is checked, make the children match the parent.
          if(isParent($(this).val())){
            var children = getChildrenFromParent($(this).val());
            for(var child in children){
              // $('.' + regions_or_groups + ' tr[option-tree-data="option-'+children[child]+'"] input').prop("checked", $(this).prop("checked"));
              $(table).find('tr[option-tree-data="option-' + children[child]+'"] input').prop("checked", $(this).prop("checked"));
            }
          }
          // Else this.val() is NOT a parent, but a child.
          else {
            // This gives us the childs parent.
            var childParent = getChildParent($(this));
            if(childParent !== undefined) {
              // child = $('tr[option-tree-data="option-' + $(this).val() + '"] input');
              child = $(table).find('tr[option-tree-data="option-' + $(this).val() + '"] input');
              // Unchecked
              if (child.prop("checked") === false) {
                $(table).find('.' + regions_or_groups + ' tr[option-tree-data="option-' + childParent + '"] input').prop("checked", false);
              }
              // Checked
              else {
                //Go over all the children of that parent.
                var childrenOfParent = getChildrenFromParent(childParent);
                var allSelected = true;
                for (child in childrenOfParent) {
                  if (!$(table).find('.' + regions_or_groups + ' tr[option-tree-data="option-' + childrenOfParent[child] + '"] input').prop("checked")) {
                    allSelected = false;
                  }
                }
                if (allSelected) {
                  $(table).find('.' + regions_or_groups + ' tr[option-tree-data="option-' + childParent + '"] input').prop("checked", true);
                }
              }
            }
          }
        });

        /**
         * Returns a parent given a child.
         *
         * @param child_name
         * @returns {string}
         */
        function getChildParent(child_name) {
          var parents = $(child_name).parents('tr').prevAll('tr.parent:first');
          if (parents.length){
            return parents.find('input').val();
          }
        }

        /**
         * Returns all the children given a parent.
         *
         * @param parent_name
         * @returns {[]}
         */
        function getChildrenFromParent(parent_name){
          var children = [];
          var options = drupalSettings.communities_group.options_tree;
          $(options[parent_name]).each(function(option_key, opt_va){
            for(var key in opt_va){
              if (key === parent_name)
                continue;
              children.push(key);
            }
          });
          return children;
        }

        /**
         * Returns true if the option is a parent.
         *
         * @param option
         * @returns {boolean}
         */
        function isParent(option){
          return $('.' + regions_or_groups + ' tr[option-tree-data="option-' + option + '"]').hasClass('parent');
        }
      });
    }
  };


})(window, Drupal, jQuery);
