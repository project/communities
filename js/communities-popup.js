(function (window, Drupal, $) {
  Drupal.behaviors.showCommunitiesPopup = {
    attach: function attach(context, settings) {

        let communitiesPopup = Drupal.dialog($('#communities-popup').once().clone(), {
          dialogClass: 'no-close',
          closeOnEscape: false,
          resizable: false,
          title: 'Select a community',
          create: function () {

            var regionList = $('<select/>', {
              name: 'Region',
              id: 'region-select',
            });

            var regions = drupalSettings.communities_popup.regions;
            // 1, 2; region keys.
            Object.keys(regions).forEach((key) => {
              var option = $('<option/>', {
                value: key,
                label: regions[key]
              });
              $(regionList).append(option);
            });

            var communitySection = $('<div/>', {
              id: 'community-section'
            });

            var communityList = $('<select/>', {
              name: 'Community',
              id: 'community-select',
            });

            var communityLabel = '<label for="community-select">Community</label>';
            var regionLabel = '<label for="region-select">Region</label>';

            $(this).append(regionLabel);
            $(this).append(regionList);

            $(this).append(communityLabel);
            $(this).once().append(communityList);

            $(this).append(communitySection);
          },
        });
        communitiesPopup.showModal();

        $('select#region-select', context).change('change', function() {
          var selectOption = $('#region-select').val();

          var dropdown = $('#community-select');
          dropdown.show();

          dropdown.find('option').remove();

          // Go over all the regions
          Object.keys(drupalSettings.communities_popup.region_map).forEach(function(key) {
            // If the selected option matches the key, populate with the key's values.
            if(key === selectOption){
              // Go over all the communities inside the region.
              Object.keys(drupalSettings.communities_popup.region_map[key]).forEach(function(v){
                dropdown.append($("<option />").val(v).text(drupalSettings.communities_popup.region_map[key][v]));
              });
            }
          });
          if($("#community-select option[value='"+drupalSettings.communities_popup.default_community+"']").length > 0){
            dropdown.val(drupalSettings.communities_popup.default_community)
          }

        });

        // Create an Update button
        var redirectButton = $('<button/>', {
          text: 'Update community',
          class: 'community-update',
          click: function (e) {
            document.cookie = 'community=' + $('#community-select').val() + ';expires=Session;path=/';
            window.location = window.location.href;
          }
        });

        $('#community-section').once().append(redirectButton);

        // On load, make sure we clear the values and default to the default ones.
        $('select#region-select').once('remove-options').each(function () {
          $('select#region-select')
            .val(drupalSettings.communities_popup.default_region)
            .trigger('change');
        });
      }
  };
})(window, Drupal, jQuery);
