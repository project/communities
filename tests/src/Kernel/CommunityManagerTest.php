<?php

namespace Drupal\Tests\communities\Functional;

use Drupal\communities\Entity\Community;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Class CommunityManagerTest.
 */
class CommunityManagerTest extends KernelTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['communities'];

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManager|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $entityTypeManager;

  /**
   * The Community manager.
   *
   * @var \Drupal\communities\CommunityManagerInterface
   */
  protected $communityManager;

  /**
   * Set up funciton.
   */
  protected function setUp() {
    parent::setUp();

    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->communityManager = \Drupal::service('community_manager');

    $this->installEntitySchema('community');
  }

  /**
   * Test getdefaultCommunity.
   */
  public function testGetDefaultCommunity() {
    $this->assertEquals('-1', $this->communityManager->getDefaultCommunity());
    Community::create(['id' => '1', 'name' => 'Kelowna'])->save();
    $this->assertEquals('1', $this->communityManager->getDefaultCommunity());
  }

}
