<?php

namespace Drupal\communities\Event;

/**
 *
 */
final class CommunitiesEvents {

  /**
   * Name of the event fired when updating a users community.
   *
   * Fired when a User updates their community.
   *
   * @Event
   */
  const COMMUNITY_UPDATE = 'communities.community.update';

}
