<?php

namespace Drupal\communities\Event;

use Drupal\communities\Entity\CommunityInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the community event.
 */
class CommunitiesEvent extends Event {


  /**
   * The community.
   *
   * @var \Drupal\communities\Entity\CommunityInterface
   */
  protected $community;

  /**
   * CommunitiesEvent constructor.
   *
   * @param \Drupal\communities\Entity\CommunityInterface $community
   */
  public function __construct(CommunityInterface $community) {
    $this->community = $community;
  }

  /**
   * Gets the community.
   */
  public function getCommunity() {
    return $this->community;
  }

}
