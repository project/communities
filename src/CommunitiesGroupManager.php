<?php

namespace Drupal\communities;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 *
 */
class CommunitiesGroupManager implements CommunitiesGroupManagerInterface {


  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface*/
  protected $entityTypeManager;

  /**
   * Constructs a CommunitiesGroupManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   *
   */
  public function getGroups() {
    $groups = $this->entityTypeManager->getStorage('communities_group')->loadMultiple();
    $group_array = [];
    foreach ($groups as $group) {
      // @todo double check
      $group_array[$group->id()] = $group->label();
    }

    return $group_array;
  }

  /**
   * Returns the communities that belong to the group.
   *
   * @param $group_name
   *   string the name of the group to load.
   *
   * @return array communities that belong in a group.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getGroupCommunitiesById($group_name) {
    $group = $this->entityTypeManager->getStorage('communities_group')->loadByProperties(['id' => $group_name]);
    $group = reset($group);
    $communities = [];
    foreach ($group->getGroupOptions() as $option) {
      $communities[$option['value']] = $option['value'];
    }
    return $communities;
  }

}
