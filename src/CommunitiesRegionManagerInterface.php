<?php

namespace Drupal\communities;

/**
 *
 */
interface CommunitiesRegionManagerInterface {

  /**
   * Returns all Regions.
   *
   * @return array
   */
  public function getRegions();

  /**
   * Returns an array of Regions to Communities.
   *
   * @return array map of regions to communities.
   */
  public function getRegionCommunitiesMap();

  /**
   * Returns an array of Region names.
   *
   * @return array the array of just region names.
   */
  public function getRegionNames();

  /**
   * Returns a Region given a Community.
   *
   * @return string region name.
   */
  public function getRegionFromCommunity($community);

  /**
   * Returns region entities.
   *
   * @return mixed
   */
  public function getRegionEntities();

  /**
   * Returns a sorted single dimensional array of entities.
   *
   * @return mixed
   */
  public function getRegionsCommunitiesArray();

}
