<?php

namespace Drupal\communities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Group entity.
 *
 * @see \Drupal\communities\Entity\Group.
 */
class GroupAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\communities\Entity\GroupInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished communities_group entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published communities_group entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit communities_group entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete communities_group entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add communities_group entities');
  }

}
