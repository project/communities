<?php

namespace Drupal\communities\Plugin\Communities\CommunityResolver;

/**
 *
 */
interface CommunityResolverInterface {

  /**
   * Decides which community to use when a user first comes to the site.
   *
   * @param $event
   *
   * @return int
   *   The community ID value.
   */
  public function resolveCommunity($event);

}
