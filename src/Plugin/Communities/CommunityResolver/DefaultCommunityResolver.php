<?php

namespace Drupal\communities\Plugin\Communities\CommunityResolver;

use Drupal\communities\Event\CommunitiesEvent;
use Drupal\communities\Event\CommunitiesEvents;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Default community resolver.
 *
 * @CommunityResolver(
 *   id = "default_community_resolver",
 *   label = @Translation("Default"),
 *   description = @Translation("Pick the community from the default list, and use this to determine the cookie.")
 * )
 */
class DefaultCommunityResolver extends CommunityResolverPluginBase {

  /**
   * Decides which community to use when a user first comes to the site.
   *
   * @return int
   *   The community ID value.
   */
  public function resolveCommunity($event) {
    $default_community = \Drupal::service('community_manager')->getDefaultCommunity();
    $default_community_entity = \Drupal::service('community_manager')->getCommunity($default_community);
    if ($default_community_entity != NULL) {
      $communityEvent = new CommunitiesEvent($default_community_entity);
      \Drupal::service('event_dispatcher')->dispatch(
        CommunitiesEvents::COMMUNITY_UPDATE, $communityEvent
      );
    }
    $response = new RedirectResponse(Url::fromRoute('<current>')->toString());
    // $response->headers->setCookie(new Cookie('community', $default_community));
    //    \Drupal::request()->getSession()->set('community', $default_community);
    /** @var \Drupal\communities\CommunityManagerInterface $community_manager */
    $community_manager = \Drupal::service('community_manager');
    $response = $community_manager->setCurrentCommunityResponse($response, $default_community);
    $event->setResponse($response);
    return $event;
  }

}
