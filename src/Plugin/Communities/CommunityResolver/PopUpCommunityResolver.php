<?php

/**
 * @file
 */

namespace Drupal\communities\Plugin\Communities\CommunityResolver;

// **
// * Pop up community resolver.
// *
// * @CommunityResolver(
// *   id = "pop_up_community_resolver",
// *   label = @Translation("Popup"),
// *   description = @Translation("Pick the community from a pop-up, and use this to determine the cookie.")
// * )
// */
// class PopUpCommunityResolver extends CommunityResolverPluginBase
// {
/**
 * Triggers the pop up via virtue of the hook...
 *
 * @param $event
 *
   * @return int
 *   The community ID value.
 */
// Public function resolveCommunity($event)
//  {
//    $attachments = [];
//    $moduleHandler = \Drupal::moduleHandler();
//    foreach ($moduleHandler->getImplementations('page_attachments') as $module) {
//      if($module == 'communities') {
//        $function = $module . '_page_attachments';
//        if($event->override_community == true){
//          $attachments['override_community'] = true;
//        }
//        $function($attachments);
//      }
//    }
//    return $event;
//  }
// }.
