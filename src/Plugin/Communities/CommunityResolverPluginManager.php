<?php

namespace Drupal\communities\Plugin\Communities;

use Drupal\communities\Annotation\CommunityResolver;
use Drupal\communities\Plugin\Communities\CommunityResolver\CommunityResolverInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 *
 */
class CommunityResolverPluginManager extends DefaultPluginManager {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Communities/CommunityResolver', $namespaces, $module_handler, CommunityResolverInterface::class, CommunityResolver::class);
    $this->setCacheBackend($cache_backend, 'communities_resolver');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    foreach (['id', 'label'] as $required_property) {
      if (empty($definition[$required_property])) {
        throw new PluginException(sprintf('The community resolver plugin %s must define the %s property.', $plugin_id, $required_property));
      }
    }
  }

  /**
   * Grabs the resolver given a setting name.
   *
   * @todo is this okay?
   * @param $setting_name
   *
   * @return string
   */
  public function getResolverFromSetting($setting_name) {
    if ($setting_name == 'community_pop_up_form') {
      return 'pop_up_community_resolver';
    }
    elseif ($setting_name == 'geo_ip') {
      return 'geo_ip_community_resolver';
    }
    else {
      return 'default_community_resolver';
    }
  }

}
