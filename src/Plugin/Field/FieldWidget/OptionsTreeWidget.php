<?php

namespace Drupal\communities\Plugin\Field\FieldWidget;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'entity_user_access_w' widget.
 *
 * @FieldWidget(
 *   id = "options_tree",
 *   label = @Translation("Check boxes/radio tree"),
 *   field_types = {
 *     "boolean",
 *     "list_string",
 *   },
 *   multiple_values = TRUE,
 * )
 */
class OptionsTreeWidget extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $options = $this->getOptions($items->getEntity());
    $selected = $this->getSelectedOptions($items);

    if (empty($options)) {
      $entity_settings = Url::fromRoute('entity.communities_region.overview_form')->toString();
      $empty_markup = "<p class='messages messages--error'>There are no entities for this type set up yet. Go <a href=" . $entity_settings . ">here</a> to create.</p>";
      $element = [
        '#type' => 'markup',
        '#markup' => $empty_markup,
      ];
    }
    else {
      // If required and there is one single option, preselect it.
      if ($this->required && count($options) == 1) {
        reset($options);
        $selected = [key($options)];
      }

      // Have to go back over the default selected array and re-assign.
      // @todo make this the getSelectedOptions...
      $default_selected = [];
      foreach ($selected as $select) {
        $default_selected[$select] = $select;
      }

      $flat_options = $this->unflatten($options);

      $element += [
        '#header' => ['Name' => $this->t('Name')],
        '#type' => 'tableselect',
        '#default_value' => $default_selected,
        '#options' => $flat_options,
        '#full_options' => $options,
      ];

      $element['#attached']['library'][] = 'communities/options-tree';
      $element['#attached']['drupalSettings']['communities_group'] = [
        'options_tree' => $options,
      ];
      $element['#attributes']['class'][] = 'options-tree';
      $element['#attributes']['class'][] = $element['#title'] == 'Community' ? 'regions' : 'groups';

      $element['#theme'] = 'options_tree';
    }
    return $element;
  }

  /**
   * @todo link title to actual node? Not sure if easily done...
   * @param $options
   */
  public function getOptionsNodeMap($options) {
    $communities = \Drupal::service('community_manager')->getCommunities();
    $regions = \Drupal::service('communities_region_manager')->getRegions();
  }

  /**
   *
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if ($element['#required'] && $element['#value'] == '_none') {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }

    if (is_array($element['#value'])) {
      $values = array_values($element['#value']);
    }
    else {
      $values = [$element['#value']];
    }

    // Filter out the 'none' option. Use a strict comparison, because
    // 0 == 'any string'.
    $index = array_search('_none', $values, TRUE);
    if ($index !== FALSE) {
      unset($values[$index]);
    }

    // @todo because we are just going to care about the communit we are saving, we don't care about the group...
    // @todo we unset it anyways when we submit.
    foreach ($values as $value_key => $element_value) {
      if (isset($element['#full_options'][$element_value]) && OptionsTreeWidget::isParent($element['#full_options'][$element_value])) {
        unset($values[$value_key]);
      }
    }

    // Transpose selections from field => delta to delta => field.
    $items = [];
    foreach ($values as $value) {
      $items[] = [$element['#key_column'] => $value];
    }
    $form_state->setValueForElement($element, $items);
  }

  /**
   * @todo Make recursive.
   * @todo make this method better named, or change the insides.
   * @todo also add tests for it.
   * @param $options
   * @return array
   */
  protected function unflatten($options) {
    $unflatened_options = [];
    foreach ($options as $option_key => $option) {
      // If the option is an array (ie parent).
      if ($this->isParent($option)) {
        $unflatened_options[$option_key] = $option_key;
        foreach ($option as $option_child_key => $option_child) {
          $unflatened_options[$option_child_key] = $option_child;
        }
      }
      else {
        $unflatened_options[$option_key] = $option;
      }
    }
    $table_select_options = [];
    foreach ($unflatened_options as $option_key => $option) {
      $table_select_options[$option_key] = [
        "Name" => new FormattableMarkup('<a href=":link">@name</a>', [':link' => '', '@name' => $option]),
        '#attributes' => [
          'option-tree-data' => 'option-' . $option_key,
          'class' => in_array($option, array_keys($options)) || in_array($option, array_values($options)) ? 'parent' : 'child',
        ],
      ];
    }
    return $table_select_options;
  }

  /**
   * Returns true or false on whether or not the element is a parent.
   *
   * @param $option
   *
   * @return bool
   */
  public static function isParent($option) {
    if (is_array($option)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function supportsGroups() {
    return TRUE;
  }

  /**
   * Returns the array of options for the widget.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity for which to return options.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {
      // Limit the settable options for the current user account.
      $options = $this->fieldDefinition
        ->getFieldStorageDefinition()
        ->getOptionsProvider($this->column, $entity)
        ->getSettableOptions(\Drupal::currentUser());

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $options = ['_none' => $empty_label] + $options;
      }

      $module_handler = \Drupal::moduleHandler();
      $context = [
        'fieldDefinition' => $this->fieldDefinition,
        'entity' => $entity,
      ];
      $module_handler->alter('options_list', $options, $context);

      // Options might be nested ("optgroups"). If the widget does not support
      // nested options, flatten the list.
      if (!$this->supportsGroups()) {
        $options = OptGroup::flattenOptions($options);
      }

      $this->options = $options;
    }
    return $this->options;
  }

}
