<?php

namespace Drupal\communities\Plugin\Block;

use Drupal\communities\Event\CommunitiesEvent;
use Drupal\communities\Event\CommunitiesEvents;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Block(
 *   id = "communities_community_autocomplete_update_block",
 *   admin_label = @Translation("Community Autocomplete Update Block"),
 *   category = @Translation("Communities")
 *   )
 */
class CommunityAutocompleteUpdateBlock extends BlockBase implements FormInterface, ContainerFactoryPluginInterface {
  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new AjaxFormBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('messenger')
    );
  }

  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * If a block should not be rendered because it has no content, then this
   * method must also ensure to return no content: it must then only return an
   * empty array, or an empty array with #cache set (with cacheability metadata
   * indicating the circumstances for it being empty).
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build() {
    return $this->formBuilder->getForm($this);

  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'community_autocomplete_update_block_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\communities\CommunityManagerInterface $community_manager */
    $community_manager = \Drupal::service('community_manager');
    $community_id = $community_manager->getCurrentCommunity();
    if (!isset($community_id)) {
      $community_id = $community_manager->getDefaultCommunity();
    }
    $community_name = $community_manager->getCommunity($community_id)->label();

    $form['community_link'] = [
      '#type' => 'markup',
      '#markup' => Markup::create('<a type="button" class="btn-hover-primary d-flex align-items-center">' . $community_name . '</a>'),
    ];
    $form['community'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'community',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'update community',
    ];
    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new RedirectResponse(\Drupal::request()->getRequestUri());
    $community = $form_state->getValue('community');
    $cookie = new Cookie('community', $community);

    // Trigger our community update event.
    $communityEvent = new CommunitiesEvent(\Drupal::service('community_manager')->getCommunity($community));
    \Drupal::service('event_dispatcher')->dispatch(
      CommunitiesEvents::COMMUNITY_UPDATE, $communityEvent
    );

    /** @var \Drupal\communities\CommunityManagerInterface $community_manager */
    $community_manager = \Drupal::service('community_manager');
    $response = $community_manager->setCurrentCommunityResponse($response, $community);

    $form_state->setResponse($response);
  }

}
