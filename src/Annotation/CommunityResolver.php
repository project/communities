<?php

namespace Drupal\communities\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CommunityResolver annotation object.
 *
 * @see \Drupal\communities\Plugin\Communities\CommunityResolverPluginManager
 * @see plugin_api
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class CommunityResolver extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
