<?php

namespace Drupal\communities;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Cookie;

/**
 *
 */
class CommunityManager implements CommunityManagerInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface*/
  protected $entityTypeManager;

  /**
   * @var CommunitiesGroupManagerInterface*/
  protected $communityGroupManager;

  /**
   * Constructs a CommunityManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CommunitiesGroupManagerInterface $community_group_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->communityGroupManager = $community_group_manager;
  }

  /**
   * {@inheritdoc}.
   */
  public function getCurrentCommunity() {
    $session = \Drupal::request()->getSession()->get('community');
    $community_cookie = \Drupal::request()->cookies->get('community');

    if (!isset($session) || empty($session) || $session != $community_cookie) {
      \Drupal::request()->getSession()->set('community', $community_cookie);
    }
    return $community_cookie;
  }

  /**
   * {@inheritdoc}
   */
  public function setCurrentCommunityResponse(&$response, $community) {
    $cookie = new Cookie('community', $community);
    $response->headers->setCookie($cookie);
    \Drupal::request()->getSession()->set('community', $community);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommunityNameFromId($community_id) {
    $entity = $this->entityTypeManager->getStorage('community')->load($community_id);
    return $entity->label();
  }

  /**
   *
   */
  public function getCommunity($community_id) {
    return $this->entityTypeManager->getStorage('community')->load($community_id);
  }

  /**
   * {@inheritdoc}.
   */
  public function getCommunities() {
    $communities = $this->entityTypeManager->getStorage('community')->loadMultiple();
    $community_array = [];
    foreach ($communities as $community) {
      // @todo check this
      $community_array[$community->id()] = $community->label();
    }

    return $community_array;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommunityFromName(string $communityName) {
    $community = $this->entityTypeManager->getStorage('community')->loadByProperties(['name' => $communityName]);
    return $community;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommunityEntities() {
    $communities = $this->entityTypeManager->getStorage('community')->loadMultiple();
    return $communities;
  }

  /**
   * Returns the method of determining the community selection.
   *
   * @return string
   */
  public function getCommunitySelectionMethod() {
    $config = \Drupal::config('communities.settings');
    $community_selection_method = $config->get('community_selection');
    return $community_selection_method;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultCommunity() {
    // @todo There is a difference between the community selection method
    // @todo and the DEFAULT community. Default community must always be present.
    // @todo any error checking?
    $default_community = \Drupal::config('communities.settings')->get('default_community');
    if ($this->getCommunity($default_community) == NULL) {
      $default_community = NULL;
    }
    if ($default_community == NULL) {
      // Set default to the first one.
      $communities = $this->getCommunityEntities();
      if (empty($communities)) {
        $default_community = '-1';
      }
      else {
        $this->setDefaultCommunity(reset($communities)->id());
      }
    }
    return $default_community;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultCommunity($community) {
    \Drupal::configFactory()->getEditable('communities.settings')->set('default_community', $community)->save();
    return $this->getDefaultCommunity();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityCommunities($entity_type, $entity_id) {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    $communities_to_return = [];
    if ($entity->get('regions_or_groups')->value == '0') {
      foreach ($entity->get('community')->getValue() as $community) {
        $communities_to_return[$community['value']] = $community['value'];
      }
    }
    else {
      foreach ($entity->get('group_options')->getValue() as $group) {
        $group = $group['value'];
        $group_communities = $this->communityGroupManager->getGroupCommunitiesById($group);
        foreach ($group_communities as $community) {
          $communities_to_return[$community] = $community;
        }
      }
    }
    return $communities_to_return;
  }

}
