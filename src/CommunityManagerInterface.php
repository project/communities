<?php

namespace Drupal\communities;

/**
 * Interface CommunityManagerInterface.
 *
 * @package Drupal\communities
 */
interface CommunityManagerInterface {

  /**
   * Returns the current community stored in the session.
   *
   * @return mixed
   */
  public function getCurrentCommunity();

  /**
   * Sets the community cookie variable in the session and cookie.
   *
   * @param $response
   * @param mixed $community
   *
   * @return mixed
   */
  public function setCurrentCommunityResponse(&$response, $community);

  /**
   * Returns a list of all the communities.
   *
   * @return array - an array keyed with the community name and value as community name.
   */
  public function getCommunities();

  /**
   * Returns community entity by id.
   *
   * @param $community_id
   *   string community id.
   *
   * @return \Drupal\communities\Entity\CommunityInterface community entity.
   */
  public function getCommunity($community_id);

  /**
   * Returns the entity name from a given string name (readable).
   *
   * @param string $entityName
   *
   * @return string entity name
   */
  public function getCommunityFromName(string $entityName);

  /**
   * Returns the entity name given the entity id.
   *
   * @param string $community_id
   *
   * @return mixed
   */
  public function getCommunityNameFromId($community_id);

  /**
   * Returns an array of community entities.
   *
   * @return array community entities.
   */
  public function getCommunityEntities();

  /**
   * Returns the method of determining the community selection.
   *
   * @return string
   */
  public function getCommunitySelectionMethod();

  /**
   * Returns the default community.
   *
   * @return string the default community.
   */
  public function getDefaultCommunity();

  /**
   * Sets the default community.
   *
   * @param $community
   *   string community ID.
   *
   * @return string the default community.
   */
  public function setDefaultCommunity($community);

  /**
   * Returns the communities belonging to an entity.
   *
   * @param $entity_type
   *   string The entity type.
   * @param $entity_id
   *   int The entity ID.
   */
  public function getEntityCommunities($entity_type, $entity_id);

}
