<?php

namespace Drupal\communities;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for Groups.
 */
class GroupTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
