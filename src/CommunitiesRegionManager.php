<?php

namespace Drupal\communities;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 *
 */
class CommunitiesRegionManager implements CommunitiesRegionManagerInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface*/
  protected $entityTypeManager;

  /**
   * @var CommunityManagerInterface*/
  protected $communityManager;

  /**
   * CommunitiesRegionManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param CommunityManagerInterface $community_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CommunityManagerInterface $community_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->communityManager = $community_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegions() {
    $regions = $this->getRegionEntities();
    $region_array = [];
    foreach ($regions as $region) {
      // @todo check this
      $region_array[$region->id()] = $region->label();
    }
    return $region_array;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegionEntities() {
    $regions = $this->entityTypeManager->getStorage('communities_region')->loadMultiple();
    return $regions;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegionNames() {
    $regions = $this->getRegionCommunitiesMap();

    // Get just the first level of regions (North BC, etc)
    $regions = array_keys($regions);
    $region_keys = [];
    foreach ($regions as $region) {
      $region_keys[$region] = $region;
    }

    return $region_keys;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegionCommunitiesMap() {
    $communities = $this->communityManager->getCommunityEntities();
    $region_map = [];
    foreach ($communities as $community) {
      $region = $community->communities_region->first();
      if ($region != NULL) {
        $region = $region->entity;
        if ($region != NULL) {
          // @todo check this
          // @todo for the time being I think we can use region label, but is there a hook we can use to make it more
          // specific to client needs?
          // @todo look into hook_options_list_alter.
          $region_map[$region->label()][$community->id()] = $community->label();
        }
      }
    }
    return $region_map;
  }

  /**
   * {@inheritdoc}
   */
  public function getRegionsCommunitiesArray() {
    $regions = $this->getRegionEntities();
    $communities = $this->communityManager->getCommunityEntities();
    uasort($communities, ['Drupal\communities\CommunitiesRegionManager', 'sortByWeight']);
    uasort($regions, ['Drupal\communities\CommunitiesRegionManager', 'sortByWeight']);
    $array = [];
    foreach ($regions as $region) {
      $array[] = $region;
      foreach ($communities as $community) {
        if ($community->communities_region->target_id == 0) {
          $community->set('communities_region', reset($regions));
        }
        if ($community->communities_region->target_id === $region->id()) {
          $array[] = $community;
        }
      }
    }

    return $array;
  }

  /**
   * Sorts by weight.
   *
   * @param $a
   * @param $b
   *
   * @return int
   */
  public static function sortByWeight($a, $b) {
    $a_weight = $a->getWeight();
    $b_weight = $b->getWeight();

    if ($a_weight == $b_weight) {
      return 0;
    }
    return ($a_weight < $b_weight) ? -1 : 1;
  }

  /**
   * Returns a Region given a Community.
   *
   * @return string region name.
   */
  public function getRegionFromCommunity($community) {
    // We have the community name, now we work backwards to find the Region that owns it.
    $region_map = $this->getRegionCommunitiesMap();
    $selected_region = '';

    foreach ($region_map as $region => $communities) {
      if (in_array($community, array_keys($communities))) {
        $selected_region = $region;
        break;
      }
    }
    return $selected_region;
  }

}
