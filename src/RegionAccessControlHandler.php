<?php

namespace Drupal\communities;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Region entity.
 *
 * @see \Drupal\region\Entity\Region.
 */
class RegionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\region\Entity\RegionInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished communities_region entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published communities_region entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit communities_region entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete communities_region entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add communities_region entities');
  }

}
