<?php

namespace Drupal\communities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Region entities.
 *
 * @ingroup region
 */
class RegionDeleteForm extends ContentEntityDeleteForm {


}
