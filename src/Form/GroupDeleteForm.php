<?php

namespace Drupal\communities\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Group entities.
 *
 * @ingroup communities
 */
class GroupDeleteForm extends ContentEntityDeleteForm {


}
