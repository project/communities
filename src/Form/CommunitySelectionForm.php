<?php

namespace Drupal\communities\Form;

use Drupal\communities\CommunityManagerInterface;
use Drupal\communities\Event\CommunitiesEvent;
use Drupal\communities\Event\CommunitiesEvents;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 *
 */
class CommunitySelectionForm extends FormBase {

  /**
   * @var \Drupal\communities\CommunityManagerInterface*/
  protected $communityManager;

  /**
   * CommunitySelectionForm constructor.
   *
   * @param \Drupal\communities\CommunityManagerInterface $community_manager
   */
  public function __construct(CommunityManagerInterface $community_manager) {
    $this->communityManager = $community_manager;
  }

  /**
   * {@inheritdoc}.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('community_manager')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'community_selection_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $this->communityManager->getCommunities();

    if (isset($_COOKIE['community'])) {
      $default_value = $_COOKIE['community'] != NULL ? $_COOKIE['community'] : reset($values);
    }
    else {
      $default_value = '';
    }

    $form['community'] = [
      '#type' => 'select',
      '#title' => $this->t('Community'),
      '#options' => $values,
      '#default_value' => $default_value,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update Community'),
    ];

    $form_state->disableCache();
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new RedirectResponse(\Drupal::request()->getRequestUri());
    $community = $form_state->getValue('community');
    $cookie = new Cookie('community', $community);

    // Trigger our community update event.
    $communityEvent = new CommunitiesEvent(\Drupal::service('community_manager')->getCommunity($community));
    \Drupal::service('event_dispatcher')->dispatch(
      CommunitiesEvents::COMMUNITY_UPDATE, $communityEvent
    );

    /** @var \Drupal\communities\CommunityManagerInterface $community_manager */
    $community_manager = \Drupal::service('community_manager');
    $response = $community_manager->setCurrentCommunityResponse($response, $community);

    // $response->headers->setCookie($cookie);
    //    \Drupal::request()->getSession()->set('community', reset($community)->id());
    $form_state->setResponse($response);
  }

}
