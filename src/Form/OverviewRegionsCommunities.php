<?php

namespace Drupal\communities\Form;

use Drupal\communities\CommunitiesRegionManagerInterface;
use Drupal\communities\CommunityManagerInterface;
use Drupal\communities\Entity\Community;
use Drupal\communities\Entity\CommunityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class OverviewRegionsCommunities extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;
  /**
   * @var \Drupal\communities\CommunityManagerInterface
   */
  private $communityManager;
  /**
   * @var \Drupal\communities\CommunitiesRegionManagerInterface
   */
  private $regionsManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;


  /**
   * The entity list builder.
   *
   * @var \Drupal\Core\Entity\EntityListBuilderInterface
   */
  protected $communityListBuilder, $regionListBuilder;

  /**
   *
   */
  public function __construct(EntityTypeManager $entity_type_manager, CommunityManagerInterface $community_manager, CommunitiesRegionManagerInterface $regions_manager, RendererInterface $renderer = NULL) {
    $this->entityTypeManager = $entity_type_manager;
    $this->communityManager = $community_manager;
    $this->regionsManager = $regions_manager;
    $this->communityListBuilder = $entity_type_manager->getListBuilder('community');
    $this->regionListBuilder = $entity_type_manager->getListBuilder('communities_region');
    $this->renderer = $renderer ?: \Drupal::service('renderer');
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('community_manager'),
      $container->get('communities_region_manager')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'overview_regions_communities';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['regions-communities'] = [
      '#type' => 'table',
      '#empty' => $this->t('No terms available.'),
      '#header' => [
        'community' => $this->t('Name'),
        'operations' => $this->t('Operations'),
        'weight' => $this->t('Weight'),
      ],
      '#attributes' => [
        'id' => 'regions-communities',
      ],
    ];

    // This returns an array sorted by weights.
    $regions_communities = $this->regionsManager->getRegionsCommunitiesArray();
    $delta = sizeof($regions_communities);
    // $community = array of community_id => communityEntity, regionEntity.
    foreach ($regions_communities as $key => $value) {
      $form['regions-communities'][$key] = [
        'community' => [],
        'operations' => [],
        'weight' => [],
        '#entity' => $value,
      ];
      $indentation = [];
      if ($value->getEntityTypeId() == 'community') {
        $indentation = [
          '#theme' => 'indentation',
          '#size' => 1,
        ];
      }

      $operations = $value instanceof Community ? $this->communityListBuilder->getOperations($value) : $this->regionListBuilder->getOperations($value);
      $form['regions-communities'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $operations,
      ];

      $form['regions-communities'][$key]['community'] = [
        '#prefix' => !empty($indentation) ? $this->renderer->render($indentation) : '',
        '#type' => 'link',
        '#title' => $value->getName(),
        // @todo fix this.
        '#url' => $value->toUrl('edit-form'),
      ];

      $id = $value->id();
      $form['regions-communities'][$key]['community']['communityid'] = [
        '#type' => 'hidden',
        '#value' => $id,
        '#attributes' => [
          'class' => ['community-id'],
        ],
      ];

      $form['regions-communities'][$key]['community']['parent'] = [
        '#type' => 'hidden',
      // This is the id? of the region.
        '#default_value' => $value->getEntityTypeId() == 'community' ? $value->communities_region->target_id : 0,
        '#attributes' => [
          'class' => [$value->getEntityTypeId() == 'communities_region' ? 'community-parent' : 'community-parent'],
        ],
      ];

      $form['regions-communities'][$key]['community']['depth'] = [
        '#type' => 'hidden',
        '#default_value' => $value->getEntityTypeId() == 'communities_region' ? 0 : 1,
        '#attributes' => [
          'class' => ['community-depth'],
        ],
      ];

      $form['regions-communities'][$key]['weight'] = [
        '#type' => 'weight',
        '#delta' => $delta,
        '#title' => $this->t('Weight for added community'),
        '#title_display' => 'invisible',
        '#default_value' => $value->getWeight(),
        '#attributes' => ['class' => ['community-weight']],
      ];

      $form['regions-communities'][$key]['#attributes']['class'][] = 'draggable';
      // This helps alleviate some weird dragging issues.
      if ($value->getEntityTypeId() == 'communities_region') {
        $form['regions-communities'][$key]['#attributes']['class'][] = 'tabledrag-root';
      }
      else {
        $form['regions-communities'][$key]['#attributes']['class'][] = 'tabledrag-leaf';
      }
    }

    $form['regions-communities']['#tabledrag'][] = [
      'action' => 'match',
      'relationship' => 'parent',
      'group' => 'community-parent',
      'subgroup' => 'community-parent',
      'source' => 'community-id',
      'hidden' => FALSE,
    ];

    $form['regions-communities']['#tabledrag'][] = [
      'action' => 'depth',
      'relationship' => 'sibling',
      'group' => 'community-depth',
      'hidden' => FALSE,
    ];

    $form['regions-communities']['#tabledrag'][] = [
      'action' => 'order',
      'relationship' => 'sibling',
      'group' => 'community-weight',
    ];

    $form['actions'] = ['#type' => 'actions', '#tree' => FALSE];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('regions-communities') as $key => $value) {
      $community = $form['regions-communities'][$key]['#entity'];
      $community->setWeight($value['weight']);
      if ($community instanceof CommunityInterface) {
        $community_region = $form_state->getValues()['regions-communities'][$key]['community']['parent'];
        if ($community_region != '0') {
          $community->set('communities_region', $community_region);
        }
        else {
          $community->set('communities_region', $community->communities_region->target_id);
        }
      }
      // @todo improve this, only need to save if changed...
      $community->save();
    }
  }

}
