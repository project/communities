<?php

namespace Drupal\communities\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CommunitySettingsForm.
 *
 * @ingroup communities
 */
class CommunitySettingsForm extends ConfigFormBase {

  /**
   * Community settings.
   *
   * @var string.
   */
  const SETTINGS = 'communities.settings';

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'community_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
    // // Set the submitted configuration setting.
      ->set('default_community', $form_state->getValue('default_community'))
      ->set('community_selection', $form_state->getValue('community_selection'))
      ->save();

    // Invalidate the system.site cache tag so our setting doesn't persist for anonymous users.
    \Drupal::service('cache_tags.invalidator')->invalidateTags(['config:system.site']);
    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Community entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $communities = \Drupal::service('community_manager')->getCommunities();

    $options = ['select_default_community' => 'Select a default Community', 'community_pop_up_form' => 'Use a pop-up form', 'geo_ip' => 'Geo-ip'];

    $default_selection = $config->get('community_selection');
    if ($default_selection == NULL) {
      $default_selection = 'select_default_community';
    }
    $form['community_selection'] = [
      '#title' => 'Default community functionality',
      '#description' => $this->t('Functionality the Communities module should use to determine the selected community.'),
      '#type' => 'radios',
      '#options' => $options,
      '#attributes' => ['name' => 'community_selection'],
      '#default_value' => $default_selection,
    ];

    $form['default_community'] = [
      '#title' => 'Default Community',
      '#description' => $this->t('The default community to use if one cannot be found.'),
      '#type' => 'select',
      '#options' => $communities,
      '#default_value' => $config->get('default_community'),
      '#states' => [
        // Show this textfield only if the radio 'other' is selected above.
        'visible' => [
          ':input[name="community_selection"]' => ['value' => 'select_default_community'],
        ],
      ],
    ];

    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [static::SETTINGS];
  }

}
