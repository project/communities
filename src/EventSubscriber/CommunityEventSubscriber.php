<?php

namespace Drupal\communities\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Response subscriber to replace the HtmlResponse with a BigPipeResponse.
 *
 * @see \Drupal\big_pipe\Render\BigPipe
 *
 * @todo Refactor once https://www.drupal.org/node/2577631 lands.
 */
class CommunityEventSubscriber implements EventSubscriberInterface {

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * The array keys are event names and the value can be:
   *
   *  * The method name to call (priority defaults to 0)
   *  * An array composed of the method name to call and the priority
   *  * An array of arrays composed of the method names to call and respective
   *    priorities, or 0 if unset
   *
   * For instance:
   *
   *  * ['eventName' => 'methodName']
   *  * ['eventName' => ['methodName', $priority]]
   *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
   *
   * @return array
   *   The event names to listen to
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['processRequest'];
    return $events;
  }

  /**
   * Processes the request. Checks to see if we should fire the display or not.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The Event to process.
   *
   * @return \Symfony\Component\HttpKernel\Event\GetResponseEvent
   *   Return ResponseEvent.
   */
  public function processRequest(GetResponseEvent $event) {
    $community_cookie = $event->getRequest()->cookies->get('community');
    $default_community = \Drupal::service('community_manager')->getDefaultCommunity();

    // Initially the community_cookie will be null.
    // When no communities exist yet, or when the module is initially enabled,
    // we set the default community to -1.
    // When a community exists, the default_community is updated, and will no
    // longer be -1.
    // This has to be like this or we will get infinite redirections.
    if ($community_cookie == NULL || ($default_community != -1 && $community_cookie == -1)) {
      $this->updateCommunity($event);
    }
    $this->displayPrompt();
    return $event;
  }

  /**
   * Updates the current community for the user.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The event to process.
   *
   * @return \Symfony\Component\HttpKernel\Event\GetResponseEvent
   *   Return event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function updateCommunity(GetResponseEvent &$event) {
    $config_selection = \Drupal::config('communities.settings')->get('community_selection');
    /** @var \Drupal\communities\Plugin\Communities\CommunityResolverPluginManager $manager */
    $manager = \Drupal::service('plugin.manager.community_resolver_manager');
    $plugin_id = $manager->getResolverFromSetting($config_selection);

    // hook_page_attachment will trigger the initial one, so no need to resolve.
    // We will use the resolver as a back-up when we cannot detect geo-ip...
    // @todo right way?
    if ($plugin_id != 'pop_up_community_resolver') {
      $resolver = $manager->createInstance($plugin_id);
      $event = $resolver->resolveCommunity($event);
    }
    return $event;
  }

  /**
   * Displays a message to the administrator that.
   *
   * They must create communities & regions.
   */
  public function displayPrompt() {
    // If the default_community is not set, or its -1, AND there are no active
    // community entities: display message.
    if (empty(\Drupal::service('community_manager')->getCommunityEntities())) {
      $community_settings = Url::fromRoute('entity.communities_region.overview_form')->toString();
      $current_url = \Drupal::request()->getRequestUri();
      // Show when on these routes.
      $community_settings_urls = [
        Url::fromRoute('entity.community.add_form')->toString(),
        Url::fromRoute('entity.communities_region.add_form')->toString(),
      ];

      // Only display the message if we are an admin, so normal users and anons
      // can't see it.
      if (in_array('administrator', \Drupal::currentUser()->getRoles()) && !in_array($current_url, $community_settings_urls)) {
        \Drupal::service('messenger')->addError($this->t('There are no Communities set up. Go <a href="' . $community_settings . '">here</a> to create.'));
      }
    }
  }

}
