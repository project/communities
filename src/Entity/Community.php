<?php

namespace Drupal\communities\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Community entity.
 *
 * @ingroup communities
 *
 * @ContentEntityType(
 *   id = "community",
 *   label = @Translation("Community"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\communities\CommunityListBuilder",
 *     "views_data" = "Drupal\communities\Entity\CommunityViewsData",
 *     "translation" = "Drupal\communities\CommunityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\communities\Form\CommunityForm",
 *       "add" = "Drupal\communities\Form\CommunityForm",
 *       "edit" = "Drupal\communities\Form\CommunityForm",
 *       "delete" = "Drupal\communities\Form\CommunityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\communities\CommunityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\communities\CommunityAccessControlHandler",
 *   },
 *   base_table = "community",
 *   data_table = "community_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer community entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/community/{community}",
 *     "add-form" = "/admin/structure/community/add",
 *     "edit-form" = "/admin/structure/community/{community}/edit",
 *     "delete-form" = "/admin/structure/community/{community}/delete",
 *     "collection" = "/admin/structure/community",
 *   }
 * )
 */
class Community extends ContentEntityBase implements CommunityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Community entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['communities_region'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel("Community Region")
      ->setDescription('The region this community should belong to.')
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setSetting('target_type', 'communities_region')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Community is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this community in relation to other communities.'))
      ->setDefaultValue(0);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * Gets the region associated with the community.
   */
  public function getRegion() {
    return $this->get('communities_region')->referencedEntities()[0];
  }

}
