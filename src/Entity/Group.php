<?php

namespace Drupal\communities\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Group entity.
 *
 * @ingroup communities
 *
 * @ContentEntityType(
 *   id = "communities_group",
 *   label = @Translation("Group"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\communities\GroupListBuilder",
 *
 *     "form" = {
 *       "default" = "Drupal\communities\Form\GroupForm",
 *       "add" = "Drupal\communities\Form\GroupForm",
 *       "edit" = "Drupal\communities\Form\GroupForm",
 *       "delete" = "Drupal\communities\Form\GroupDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\communities\GroupHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\communities\GroupAccessControlHandler",
 *   },
 *   base_table = "communities_group",
 *   data_table = "communites_group_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer group entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/communities_group/{communities_group}",
 *     "add-form" = "/admin/structure/communities_group/add",
 *     "edit-form" = "/admin/structure/communities_group/{communities_group}/edit",
 *     "delete-form" = "/admin/structure/communities_group/{communities_group}/delete",
 *     "collection" = "/admin/structure/communities_group",
 *   }
 * )
 */
class Group extends ContentEntityBase implements GroupInterface {
  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Group entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['group_options'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Group members'))
      ->setDescription(t('Select the regions and communities that belong in this group.'))
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_tree',
        'weight' => -4,
      ])
      ->setCardinality(-1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('allowed_values_function', 'communities_allowed_values_function_regions')
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Group is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * Gets the group options.
   *
   * @return array
   *   The group options.
   */
  public function getGroupOptions() {
    return $this->get('group_options')->getValue();
  }

}
