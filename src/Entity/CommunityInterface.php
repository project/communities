<?php

namespace Drupal\communities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Community entities.
 *
 * @ingroup communities
 */
interface CommunityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Community name.
   *
   * @return string
   *   Name of the Community.
   */
  public function getName();

  /**
   * Sets the Community name.
   *
   * @param string $name
   *   The Community name.
   *
   * @return CommunityInterface
   *   The called Community entity.
   */
  public function setName($name);

  /**
   * Gets the Community creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Community.
   */
  public function getCreatedTime();

  /**
   * Sets the Community creation timestamp.
   *
   * @param int $timestamp
   *   The Community creation timestamp.
   *
   * @return CommunityInterface
   *   The called Community entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the community weight.
   *
   * @return int
   *   The community weight.
   */
  public function getWeight();

  /**
   * Sets the community weight.
   *
   * @param int $weight
   *   The community weight.
   *
   * @return $this
   */
  public function setWeight($weight);

  /**
   * Gets the region associated with the community.
   */
  public function getRegion();

}
