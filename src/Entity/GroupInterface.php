<?php

namespace Drupal\communities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 *
 */
interface GroupInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Group name.
   *
   * @return string
   *   Name of the Group.
   */
  public function getName();

  /**
   * Sets the Group name.
   *
   * @param string $name
   *   The Group name.
   *
   * @return GroupInterface
   *   The called Group entity.
   */
  public function setName($name);

  /**
   * Gets the Group creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Group.
   */
  public function getCreatedTime();

  /**
   * Sets the Group creation timestamp.
   *
   * @param int $timestamp
   *   The Group creation timestamp.
   *
   * @return GroupInterface
   *   The called Group entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the group options.
   *
   * @return array
   *   The group options.
   */
  public function getGroupOptions();

}
