<?php

namespace Drupal\communities\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 *
 */
interface RegionInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Region name.
   *
   * @return string
   *   Name of the Region.
   */
  public function getName();

  /**
   * Sets the Region name.
   *
   * @param string $name
   *   The Region name.
   *
   * @return RegionInterface
   *   The called Region entity.
   */
  public function setName($name);

  /**
   * Gets the Region creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Region.
   */
  public function getCreatedTime();

  /**
   * Sets the Region creation timestamp.
   *
   * @param int $timestamp
   *   The Region creation timestamp.
   *
   * @return RegionInterface
   *   The called Region entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the region weight.
   *
   * @return int
   *   The region weight.
   */
  public function getWeight();

  /**
   * Sets the region weight.
   *
   * @param int $weight
   *   The region weight.
   *
   * @return $this
   */
  public function setWeight($weight);

}
