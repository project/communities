<?php

namespace Drupal\communities;

/**
 * Interface CommunitiesGroupManagerInterface.
 *
 * @package Drupal\communities
 */
interface CommunitiesGroupManagerInterface {

  /**
   * Returns all groups.
   *
   * @return array
   *   all group entities.
   */
  public function getGroups();

  /**
   * Returns the communities that belong to the group.
   *
   * @param string $group_name
   *   String the name of the group to load.
   *
   * @return array
   *   communities that belong in a group.
   */
  public function getGroupCommunitiesById($group_name);

}
